# Repository tasks

This tool can be run in a scheduled pipeline.
It can modify gitlab repositories and create merge requests for the modifications.

## Config

|Environment variable|Description|Required|
|---|---|---|
|TASKS_FILE|Path to the json file which defines the tasks|yes|
|PRIVATE_TOKEN|A gitlab access token with read and write access|yes|
|COMMIT_USER_NAME|The gitlab user name which should be used in the commit|yes|
|COMMIT_USER_EMAIL|The gitlab user email which should be used in the commit|yes|
|CI_API_V4_URL|The gitlab api endpoint|no|

### tasks.json file

```json
[
    {
        "projectId": "<project id from gitlab project>",
        "actions": [  // each action will be executed for the project (own clean branch)
            {
                // branch name to commit changes
                "branch": "bot/update-client", 
                
                // the script to execute
                "script": "./test.sh",
                
                // Information for the MR
                "mr": {
                    "commitMessage": "Update client",
                    "title": "Update client",
                    "description": "Test 1233\nNewLine\n\n- [ ] 1\n- [ ] 2",
                    // Gitlab labels to add
                    "labels": [
                        "Test"
                    ],
                    // Assign gitlab users
                    "assign": [
                        "@renkor"
                    ],
                    "mergeWhenPipelineSucceeds": false
                }
            }
        ]
    }
]
```

## Run tasks

Call `run-tasks` (no path needed, it is part of `$PATH`)

### Gitlab example


```yaml
run:
  stage: run
  image: registry.gitlab.com/renkor/repository-tasks:v1-0-2
  variables:
    TASKS_FILE: "./tasks.json"
    PRIVATE_TOKEN: "<CI VARIABLE>"
    COMMIT_USER_NAME: "myuser"
    COMMIT_USER_EMAIL: "myuser@example.com"
  script:
    - run-tasks
```
