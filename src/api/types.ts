
export type Project = {
    id: number,
    path_with_namespace: string
    default_branch: string
    http_url_to_repo: string
}

export type MergeRequest = {
    id: number
    project_id: number
    title: string
    state: string
    source_branch: string
    target_branch: string
    merge_status: string
    has_conflicts: boolean
}