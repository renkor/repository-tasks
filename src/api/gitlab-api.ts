import { MergeRequest, Project } from "./types";
import querystring from 'querystring'

import config from '../utils/config'

export async function getProjectById(id: number | string) {
    const res = await fetch(`${config.gitlabApi}/projects/${id}`, {
        headers: {
            "PRIVATE-TOKEN": config.apiKey
        }
    })
    if (!res.ok || res.status !== 200) throw Error(`Could not find project with id ${id} (${res.statusText})`)
    return (await res.json()) as Project
}

export function getCloneUrl(gitHttpAddress: string) {
    const isHttps = gitHttpAddress.startsWith("https://")
    return gitHttpAddress.replace(/^http[s]?:\/\//, `${isHttps ? 'https' : 'http'}://oauth2:${config.apiKey}@`)
}

export async function getMergeRequest(projectId: number | string, sourceBranch: string) {
    const query = querystring.stringify({
        state: 'opened',
        source_branch: sourceBranch
    })

    const res = await fetch(`${config.gitlabApi}/projects/${projectId}/merge_requests?${query}`, {
        headers: {
            "PRIVATE-TOKEN": config.apiKey
        }
    })
    if (!res.ok || res.status !== 200) throw Error(`Could not get merge requests for project ${projectId} (${res.statusText})`)
    return (await res.json()) as Array<MergeRequest>
}