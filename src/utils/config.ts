import dotenv from "dotenv";
import process from "process";
import fs from "fs";

dotenv.config({
  override: true,
});

export type Task = {
  projectId: number;
  actions: Array<Action>;
};

export type Action = {
  branch: string;
  script: string;
  mr: MergeRequestConfig
};

export type MergeRequestConfig = {
  description: string;
  title: string;
  commitMessage: string;
  labels?: Array<string>;
  assign: Array<string>;
  mergeWhenPipelineSucceeds?: boolean
};

let tasks: Array<Task> = [];

if (process.env.TASKS_FILE) {
  const buffer = fs.readFileSync(process.env.TASKS_FILE, {
    encoding: "utf-8",
  });
  tasks = JSON.parse(buffer) as Array<Task>;
}

const config = {
  apiKey: process.env.PRIVATE_TOKEN || "",
  gitlabApi: process.env.CI_API_V4_URL || "https://gitlab.com/api/v4",
  user: {
    email: process.env.COMMIT_USER_EMAIL || "<>",
    name: process.env.COMMIT_USER_NAME || "undefined",
  },
  projects: tasks,
};

export default config;
