import { Serializable, spawn } from 'node:child_process'

export type RunProcessResult = {
    exitCode: number
    stdout: string
}

export function runProcessAsync(command: string, args: Array<string>, workingDir?: string, ioCallback?: (stream: 'STDOUT' | 'ERROR', data: Serializable) => void) {
    return new Promise<RunProcessResult>((resolve, reject) => {
        const p = spawn(command, args, {
            cwd: workingDir,
        })

        let stdout = ""
        let stderr = ""

        p.stdout.on('data', (data) => {
            const toSend = `${data}`
            stdout += toSend
            if (toSend.trim().length === 0) return
            ioCallback?.('STDOUT', toSend)
        })

        p.stderr.on('data', (data) => {
            const toSend = `${data}`
            stderr += toSend
            if (toSend.trim().length === 0) return
            ioCallback?.('ERROR', toSend)
        })

        p.on('exit', (exitCode) => {
            if (exitCode === 0) return resolve({
                exitCode,
                stdout,
            })

            return reject({
                command: `${command} ${args?.join(' ')}`,
                workingDir,
                exitCode,
                stdout,
                stderr
            })
        })
    })
}