import { getCloneUrl, getMergeRequest, getProjectById } from "./api/gitlab-api";
import { runProcessAsync } from "./utils/runProcessAsync";
import config from "./utils/config";
import {
  cleanup,
  cloneRepo,
  setupGit,
  checkoutBranch,
  resetBranch,
  commitAndPushChanges,
} from "./actions/git";
import { runScript } from "./actions/runScript";
import { checkIfBranchIsBehindMaster } from "./actions/git/checkIfBranchIsBehindMaster";

process.once("SIGINT", (e) => {
  console.log("SIGINT received...");
  process.exit(1);
});

(async () => {
  console.log("Delete old repos");
  await runProcessAsync("rm", ["-rf", "./cloned"]).catch(() => {});

  await setupGit(config.user.name, config.user.email);

  for (let projectDef of config.projects) {
    console.log(" ");
    console.log("#########################");
    console.log("Handle project", projectDef.projectId);
    console.log("#########################");

    if (!projectDef.actions) continue;

    const projectId = projectDef.projectId || "undefined";
    const project = await getProjectById(projectId);
    console.log("Name: ", project.path_with_namespace);
    const outDir = `./cloned/${project.id}`;
    const cloneUrl = getCloneUrl(project.http_url_to_repo);

    await cloneRepo(cloneUrl, ".", outDir);

    for (let action of projectDef.actions) {
      console.log("------- Execute ---------");

      if (action.branch === project.default_branch) {
        console.error("ERROR: Actions for default branch are not allowed")
        continue
      }

      try {
        await cleanup(outDir);
        const branchIsNew = await checkoutBranch(action.branch, project.default_branch, outDir);

        let forcePush = false;

        if (!branchIsNew)
        {
          if (await checkIfBranchIsBehindMaster(action.branch, project.default_branch, outDir)) {
            await resetBranch(project.default_branch, outDir, "branch behind master");
            forcePush = true;
          }
          else {
            const mrs = await getMergeRequest(project.id, action.branch);

            if (mrs.length === 0) {
              await resetBranch(project.default_branch, outDir, "no active merge request");
              forcePush = true;
            }
          }
        }

        await runScript(action.script, ".", outDir);

        await commitAndPushChanges(
          action.branch,
          project.default_branch,
          forcePush,
          outDir,
          action.mr
        );
      } catch (e) {
        console.error("ERROR:", JSON.stringify(e, null, 2));
      }
    }
  }
})();
