import { existsSync } from "fs";
import { runProcessAsync } from "../utils/runProcessAsync";

export async function runScript(
  script: string,
  workingDir: string,
  targetDir: string
) {
  if (!script || !existsSync(script)) {
    console.log(" ");
    console.log("WARN: Script not found");
    console.log(" ");
    return;
  }

  console.log("Run script", script);
  await runProcessAsync("bash", [script, targetDir], workingDir, (s, d) =>
    console.log(d)
  );
}
