import { MergeRequestConfig } from "../../utils/config";
import { runProcessAsync } from "../../utils/runProcessAsync";

export async function push(
  branch: string,
  targetBranch: string,
  force: boolean,
  workingDir: string,
  mr?: MergeRequestConfig
) {
  console.log("Push changes force: ", force);

  let pushArgs = ["push", "--set-upstream", "origin", branch];

  if (mr) {
    pushArgs = [
      ...pushArgs,
      "-o",
      "merge_request.create",
      "-o",
      `merge_request.target_branch=${targetBranch}`,
      "-o",
      `merge_request.title=${mr.title || 'undefined'}`,
      "-o",
      "merge_request.remove_source_branch",
    ];

    if (!!mr.mergeWhenPipelineSucceeds) {
      pushArgs.push('-o')
      pushArgs.push('merge_request.merge_when_pipeline_succeeds')
    }

    if (mr.description) {
        pushArgs.push("-o")
        pushArgs.push(`merge_request.description=${mr.description.replace(/(?:\r\n|\r|\n)/g, '<br>')}`)
    }

    if (mr.labels) {
      for (let label of mr.labels) {
        if (!label) continue
        pushArgs.push("-o");
        pushArgs.push(`merge_request.label=${label}`);
      }
    }

    if (mr.assign) {
      for (let user of mr.assign) {
        if (!user) continue
        pushArgs.push("-o");
        pushArgs.push(`merge_request.assign=${user}`);
      }
    }
  }

  if (force) {
    pushArgs.push("--force");
  }

  await runProcessAsync("git", pushArgs, workingDir);
}
