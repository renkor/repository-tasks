import { MergeRequestConfig } from "../../utils/config";
import { runProcessAsync } from "../../utils/runProcessAsync";
import { addChanges } from "./addChanges";
import { push } from "./push";

export async function commitAndPushChanges(
  branch: string,
  targetBranch: string,
  force: boolean,
  workingDir: string,
  mr: MergeRequestConfig
) {
  const commitNeeded = await addChanges(workingDir);

  if (!commitNeeded) {
    console.log("No files changed");

    if (force) {
      console.log("Rebase branch");
      await push(branch, targetBranch, true, workingDir);
    }

    return;
  }

  console.log("Commit changes");
  await runProcessAsync(
    "git",
    ["commit", "-m", mr.commitMessage],
    workingDir,
    (s, d) => console.log(d)
  );
  await push(branch, targetBranch, force, workingDir, mr);
}
