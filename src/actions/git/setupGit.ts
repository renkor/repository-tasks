import { runProcessAsync } from "../../utils/runProcessAsync";

export async function setupGit(user: string, email: string) {
    console.log("Setup git email to ", email);
    await runProcessAsync("git", [
      "config",
      "--global",
      "user.email",
      email || "undefined",
    ]);
  
    console.log("Setup git user to ", user);
    await runProcessAsync("git", [
      "config",
      "--global",
      "user.name",
      user || "undefined",
    ]);
}