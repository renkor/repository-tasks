import { runProcessAsync } from "../../utils/runProcessAsync";

export async function checkIfBranchIsBehindMaster(
  branch: string,
  defaultBranch: string,
  workingDir: string
) {
  console.log(`Check if '${branch}' is behind '${defaultBranch}'`);
  try {
    const { stdout } = await runProcessAsync(
      "git",
      [
        "rev-list",
        "--left-right",
        "--count",
        `origin/${defaultBranch}...${branch}`,
      ],
      workingDir
    );
    return parseInt(stdout.split(' ')[0]) > 0;
  } catch (e) {
    console.log(`Branch '${branch}' does not exits, create new one`);
    await runProcessAsync(
      "git",
      ["checkout", "-b", branch, defaultBranch],
      workingDir
    );
    return true;
  }
}
