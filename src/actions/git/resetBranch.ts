import { runProcessAsync } from "../../utils/runProcessAsync";

export async function resetBranch(
  defaultBranch: string,
  workingDir: string,
  reason: string
) {
  console.log("Reset branch: ", reason);
  await runProcessAsync(
    "git",
    ["reset", "--hard", `origin/${defaultBranch}`],
    workingDir
  );
}
