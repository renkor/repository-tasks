import { runProcessAsync } from "../../utils/runProcessAsync";

export async function cleanup(dir: string) {
  console.log("Clean current git state");
  await runProcessAsync("git", ["checkout", "."], dir);
  await runProcessAsync("git", ["clean", "-xfd"], dir);
}
