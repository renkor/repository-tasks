import { runProcessAsync } from "../../utils/runProcessAsync";

export async function addChanges(workingDir: string) {
  console.log("Add changes");
  let commitNeeded = false;
  await runProcessAsync("git", ["add", ".", "-v"], workingDir, (stream, data) => {
    if (stream === "STDOUT") commitNeeded = true;
    console.log(data);
  });
  return commitNeeded
}
