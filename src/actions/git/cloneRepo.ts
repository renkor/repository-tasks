import { runProcessAsync } from "../../utils/runProcessAsync";

export async function cloneRepo(cloneUrl: string, workingDir: string, outDir: string) {
  console.log("Clone project");
    await runProcessAsync("git", ["clone", cloneUrl, outDir], workingDir, (s, d) =>
      console.log(d)
    );
}
