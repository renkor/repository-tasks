import { runProcessAsync } from "../../utils/runProcessAsync";

export async function checkoutBranch(
  branch: string,
  defaultBranch: string,
  workingDir: string
) {
  console.log("Checkout branch ", branch);
  try {
    await runProcessAsync("git", ["checkout", branch], workingDir);
    return false
  } catch (e) {
    console.log("Branch does not exits, create new one");
    await runProcessAsync(
      "git",
      ["checkout", "-b", branch, defaultBranch],
      workingDir
    );
    return true
  }
}
