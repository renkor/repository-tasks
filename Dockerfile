FROM node:lts-alpine
WORKDIR /repository-tasks

RUN apk add git curl wget bash jq

COPY ./docker/run-tasks /usr/local/bin/run-tasks
RUN chown root:node /usr/local/bin/run-tasks && chmod g+x /usr/local/bin/run-tasks

COPY ./package*.json /repository-tasks/
RUN npm ci --omit=dev && \
    rm /repository-tasks/package*.json 

COPY ./dist /repository-tasks

WORKDIR /app

RUN chown -R node:node /app

USER node

ENTRYPOINT [""]
CMD [ "run-tasks" ]